import React from 'react';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import IconButtons from '@material-ui/core/IconButton';

const useStyles = makeStyles((theme) => ({
    Background: {
        backgroundColor: "#373837",
        height: "330px",


    },
    socialIcon: {
        '& > *': {
            // margin: theme.spacing(1),
            width: theme.spacing(6),
            height: theme.spacing(6),
        },
        position: "relative",
        left: "400px"
    },
    footer: {
        position: "relative",
        top: "120px"
    },
    copyRight: {
        position: "relative",
        top:"150px"
        
    },
    Arrow: {
        width: theme.spacing(8),
        height: "60px",
        position: "relative",
        bottom:"60px"
    },
    ArrowIcons:{
        width:theme.spacing(6),
        height:"100px",
        position:"relative",
        bottom:"45px"
    }
}));


export default function FooterComponent() {
    const classes = useStyles();
    return (
        <>
            <div className={classes.Background}>
                <Grid className={classes.footer} container spacing={0}>
                    <Grid item md={3}>
                        <Typography>hamalniraj5@gmail.com</Typography>
                        <Typography>9815689193,9865783603</Typography>
                        <Typography>Sankhamul,Lalitpur</Typography>
                    </Grid>
                    <Grid item md={7}>
                        <div className={classes.socialIcon}>
                            <IconButtons>
                                <FacebookIcon htmlColor="#0C9BF8" ><Link to="https://www.facebook.com/hamal.wolverine21"></Link></FacebookIcon>
                            </IconButtons>
                            <IconButtons>
                                <InstagramIcon htmlColor="#F70A47"
                                ></InstagramIcon>
                            </IconButtons>
                            <IconButtons>
                                <TwitterIcon htmlColor="#1FCFF6"></TwitterIcon>
                            </IconButtons>

                        </div>
                    </Grid>
                </Grid>
                    <IconButtons className={classes.Arrow} >
                        <ArrowUpwardIcon className={classes.ArrowIcons}  htmlColor="#DAD4CF"></ArrowUpwardIcon>
                    </IconButtons>
                
                <Typography className={classes.copyRight} >Nirajan Hamal©2020</Typography>
            </div>

        </>
    )
}
