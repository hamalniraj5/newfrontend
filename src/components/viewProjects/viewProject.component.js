import React, { Component } from 'react'
import { httpClient } from '../../utils/httpClient';
import notify from '../../utils/notify';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
// import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { LoaderComponent } from '../common/loader';
// import { CardComponent } from '../common/card/card';
// import CardComponent from '../common/card/card';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';

const IMG_URL = process.env.REACT_APP_IMG_URL

export default class ViewProjectComponent extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            projects: []
        }
    }

    componentDidMount() {
        this.setState({
            isLoading: true
        })
        httpClient.get('/projects', {}, true)
            .then(data => {
                console.log("data in view", data);
                this.setState({
                    projects: data.data
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    render() {

        const root = {
            maxWidth: 345,
            // paddingLeft: "100px",
            margin: "50px",
            padding: "20px",
            textAlign: 'center',

        };
        const media = {
            // height: 0,
            // paddingTop: '56.25%', // 16:9

        };

        let myContent = (this.state.projects || []).map((item, i) => (

            <>


                <Card style={root}>
                    <CardHeader
                        action={
                            <IconButton aria-label="settings">
                                <MoreVertIcon />
                            </IconButton>
                        }
                        title={item.projectName}

                    ></CardHeader>

                    <CardMedia
                        style={media}
                        // image={`${IMG_URL}/${item.image}`}
                        title={item.image}
                    ><img
                        src={`${IMG_URL}/${item.image}`}
                        alt="product.jpg"
                        width="345px"
                    ></img></CardMedia>
                    <CardContent>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {item.description}
                        </Typography>
                    </CardContent>
                </Card>

            </>

        ))

        let maindiv = this.state.isLoading
            ? <LoaderComponent></LoaderComponent>
            : <>
                {myContent}
            </>


        return (
            <>
                <Typography
                    variant="h3"
                >View Projects</Typography>
                <Divider></Divider>
                <div>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item sm={12} md={4}>

                            {maindiv}

                        </Grid >

                    </Grid >
                </div>

            </>
        )
    }
}
