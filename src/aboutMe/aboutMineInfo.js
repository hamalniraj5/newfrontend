import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import { lighten, withStyles, makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import LinearProgress from '@material-ui/core/LinearProgress';


const useStyles = makeStyles((theme) => ({
    large: {
        width: theme.spacing(40),
        height: theme.spacing(40)
    },
    aboutDesign: {
        paddingTop: "80px",
        paddingLeft: "100px",
        paddingRight: "100px"

    },

    progressBar: {
        paddingTop: "100px",
        paddingLeft: "100px",
        paddingRight: "100px"
    },
    typograpHeight: {
        position: "relative",
        bottom: "18px"
    }


}));

const BorderLinearProgress = withStyles({
    root: {
        height: 15,
        backgroundColor: lighten('#ff6c5c', 0.5),
    },
    bar: {
        borderRadius: 30,
        backgroundColor: 'blue',
    },
})(LinearProgress);




export default function AboutMineInfo() {
    const classes = useStyles();
    return (
        <div>
            <Typography variant="h3">About</Typography>
            <Divider></Divider>
            <Grid className={classes.aboutDesign} container spacing={0}>
                <Grid item md={6}>
                    <Avatar className={classes.large} alt="Hamal Nirajan" src="/image/niraj.jpg" />

                </Grid>
                <Grid item md={6}>
                    <Typography>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur</Typography>
                </Grid>
            </Grid>
            <div>
                <Grid className={classes.progressBar} container spacing={0}>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">Html</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={50} />
                    </Grid>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">css</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={80} />
                    </Grid>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">BootStrap</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={95} />
                    </Grid>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">JavaScript</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={65} />
                    </Grid>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">Mongo DB</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={80} />
                    </Grid>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">Express js</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={95} />
                    </Grid>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">React js</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={85} />
                    </Grid>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">Node js</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={65} />
                    </Grid>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">Material-UI</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={80} />
                    </Grid>
                    <Grid item md={4}>
                        <Typography className={classes.typograpHeight} variant="h4">React Bootstrap</Typography>
                    </Grid>
                    <Grid item md={8}>
                        <BorderLinearProgress
                            variant="determinate"
                            color="primary"
                            value={95} />
                    </Grid>

                </Grid>
            </div>

        </div>
    )
}
