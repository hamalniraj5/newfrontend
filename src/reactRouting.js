import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import LoginComponent from '../src/components/users/login/loginUser.component';
import RegisterComponent from '../src/components/users/register/registerUser.component';
import AdminDashboard from './components/admin/navbar/navDashboard.admin';
import AddProjectComponent from './components/admin/addProject/addProjects';
import ViewProjects from '../src/components/viewProjects/viewProject.component';
import UserDashboard from '../src/userDashboard/userDashboard';

export default function ReactRouting() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={UserDashboard}></Route>
                <Route path="/loginUser" component={LoginComponent}></Route>
                <Route path="/registerUser" component={RegisterComponent}></Route>
                <Route path="/adminDashboard" component={AdminDashboard}></Route>
                <Route path="/addProject" component={AddProjectComponent}></Route>
                <Route path="/viewProjects" component={ViewProjects}></Route>

            </Switch>

        </BrowserRouter>
    )
}
