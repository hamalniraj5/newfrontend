import React from 'react';
import NavbarComponent from '../common/navbar/navbar';
import TextEffect from '../../src/3ddesign/design';
import AboutMineComponent from '../aboutMe/aboutMineInfo';
import ContactComponent from '../contactMe/contactMe.component';
import FooterComponent from '../common/footer/footer';


export default function UserDashboard() {
    return (
        <>
            <div>
                <NavbarComponent></NavbarComponent>
                <TextEffect></TextEffect>
            </div>
            <div className="About">
                <AboutMineComponent></AboutMineComponent>
            </div>
            <div className="Contact">
                <ContactComponent></ContactComponent>
            </div>
            <div className="Footer">
                <FooterComponent></FooterComponent>
            </div>
        
        </>
    )
}
