import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import TextAreaAutoSize from '@material-ui/core/TextareaAutosize';
import { Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import { httpClient } from '../../../utils/httpClient';
import notify from '../../../utils/notify';


const BaseUrl = `${process.env.REACT_APP_BASE_URL}/projects`;




export default class AddProjectComponent extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                projectName: '',
                description: '',
                status: '',
                image: ''
            }
        }
    }

    // fileSelectedHandler=(e)=>{
    //     console.log(e.target.files[0]);
    // }


    handleChange = (e) => {
        let { type, name, value } = e.target;
        // console.log("e.target.files", e.target.files[0]);
        // console.log('name>>>>>', name);
        // console.log('value>>>', value);
        // console.log('preserved state>>>>>>', this.state.data);
        if (type === 'file') {
            value = e.target.files;
        }

        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value,
                // image:e.target.files[0]

            }
        }), () => {

        })
        // console.log("Preserved data on state", this.state.data);

    }


    handleSubmit = e => {
        e.preventDefault();

        // const fd = new fo
        // httpClient.post('/projects')
        httpClient.upload("POST", `${BaseUrl}?token=${localStorage.getItem('token')}`, this.state.data, this.state.data.image)
            .then(response => {
                console.log("dataaaaa success", response);
                notify.showInfo('Project added successfully');
                this.props.history.push('/viewProjects');
            })
            .catch(err => {
                console.log("errror file", err);
                notify.handleError(err);

            })
    }

    render() {



        // const handleChange = (e) => {
        //     let { type, name, value } = e.target;

        //     if (type === 'files') {
        //         value = e.target.files[0];
        //     }
        //     setState(prev => ({ ...prev, [name]: value }))
        // }
        // console.log("project add props", props);
        // console.log("Preserved data on state", state);

        // const handleSubmit = (e) => {
        //     // debugger
        //     e.preventDefault();
        //     console.log("state is passed", state);
        //     const fd = new FormData();
        //     fd.append('image', state, state.image);


        // const files = state.image;
        // delete state.image




        return (
            <div>
                <Typography variant="h4">Add Projects</Typography>
                <Divider></Divider><br></br>

                <FormControl>

                    <InputLabel id="ProjectName">Project Status</InputLabel>
                    <Select
                        labelId="ProjectName"
                        id="projectStatus"
                        name="status"
                        onChange={this.handleChange}
                        value={this.state.data.value}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value="Completed">Completed</MenuItem>
                        <MenuItem value="Ongoing">Ongoing</MenuItem>

                    </Select><br></br>
                    <TextField
                        label="Projects Name"
                        type="text"
                        name="projectName"
                        variant="outlined"
                        onChange={this.handleChange}
                    /><br></br>

                    <TextAreaAutoSize
                        rowsMin={4}
                        placeholder="Description"
                        name="description"
                        onChange={this.handleChange} /><br></br>


                    <input

                        type="file"
                        name="image"
                        onChange={this.handleChange}

                    ></input>
                    <br></br>

                    <Button
                        variant="contained"
                        onClick={this.handleSubmit}
                    >save</Button><br></br>

                </FormControl>
            </div>



        )
    }
}

