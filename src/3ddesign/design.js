import React from 'react';
import './design.component.css';

export default function design() {
    return (
        <div>
            <section>
                <video  src="/image/smoke.mp4" autoPlay muted></video>
                
                <h1>
                    <span>W</span>
                    <span>E</span>
                    <span>B</span>
                    <span>D</span>
                    <span>E</span>
                    <span>V</span>
                    <span>E</span>
                    <span>L</span>
                    <span>O</span>
                    <span>P</span>
                    <span>E</span>
                    <span>R</span>
                </h1>
            </section>

        </div>
    )
}
