import axios from 'axios';

const http = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    responseType: 'json'
})

const getHeaders = (isSecure = false) => {
    let options = {
        'Content-Type': 'application/json'
    }
    if (isSecure) {
        options['Authorization'] = localStorage.getItem('token');
    }
    return options;
}

function get(url, { params = {} } = {}, isSecure = false) {

    return http.get(url, {
        params,
        headers: getHeaders(isSecure),
    })
    // .then(response => {
    //     resolve(response.data);
    // })
    // .catch(err => {
    //     reject(err.response.data);
    // })
    // })

}

function post(url, data = {}, { params = {} } = {}, isSecure = false) {
    return http.post(url, data, {
        params,
        headers: getHeaders(isSecure)
    });

}

function upload(method, url, data, files) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        var formData = new FormData();


        if (files.length) {
            formData.append('image', files[0], files[0].name);
        }

        for (let key in data) {
            formData.append(key, data[key]);
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                console.log("request response cycle is completed", xhr.response);
                if (xhr.status === 200) {
                    // console.log("req-res cycle compleeted success....data.", xhr.response);
                    resolve(xhr.response);
                }
                else {
                    console.log("failure>>>>>", xhr.response);
                    reject(xhr.response);
                }
            }
        }

        xhr.open(method, url, true);
        xhr.send(formData);
    })

}

export const httpClient = {
    get,
    post,
    upload
}