import React from 'react';

import './App.css';
import 'typeface-roboto';
import ReactRouting from '../src/reactRouting';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <div className="App">
      <ToastContainer></ToastContainer>
      <ReactRouting></ReactRouting>
    </div>
  );
}

export default App;
