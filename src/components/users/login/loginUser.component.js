
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import { TextField, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import { httpClient } from '../../../utils/httpClient';
import notify from '../../../utils/notify';
import { withRouter } from 'react-router-dom';






const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        paddingTop: "120px"
    },

    margin: {
        margin: theme.spacing(1),
    },
    withoutLabel: {
        marginTop: theme.spacing(3),
    },
    textField: {
        width: '25ch',
    },
}));

const InputAdornments = function (props) {

    const classes = useStyles();
    const [state, setState] = React.useState({

        username: '',
        password: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setState(prev => ({ ...prev, [name]: value }))
    }
    console.log("state", state);
    console.log("props.....", props);

    const registerForm = (history) => {
        history.push('/registerUser')
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log("this state", state);
        httpClient.post('/auth/adminlogin', state, {}, true)
            .then(response => {
                console.log("data is>>>>>>", response);
                notify.showSuccess(`Welcome ${response.data.user.username}`);
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('user', JSON.stringify(response.data.user));
                props.history.push("/adminDashboard");
            })
            .catch(err => {
                console.log("http error", err);
                console.log(" error", err.response);
                notify.handleError(err);
            })
        // httpClient.post('/auth/adminlogin', state)
        //     .then(response => {
        //         console.log('response data', response.data);
        //         // console.log('history', history);
        //         // console.log("props.history",props.history);
        //         localStorage.setItem('token', response.data.token)
        //         localStorage.setItem('user', JSON.stringify(response.data.user));
        //         history.push('/registerUser');
        //     })
        // .catch(err => {
        //     notify.handleError(err);
        // })
        // .finally(() => {

        // })
    }


    return (
        <div className={classes.root}>
            <div>
                <Typography variant="h3">Login</Typography>
                <Divider></Divider><br></br>

                <FormControl>
                    <TextField
                        type="text"
                        variant="outlined"
                        label="Username"
                        name="username"
                        onChange={handleChange} /><br></br>
                    <TextField variant="outlined"
                        type="password"
                        label="Password"
                        name="password"
                        onChange={handleChange} /><br></br>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleSubmit}
                    >Login</Button><br></br>
                    <Button onClick={() => registerForm(props.history)} >Sign up for new User</Button>

                </FormControl>


            </div>
        </div>
    );
}
export default withRouter(InputAdornments);
