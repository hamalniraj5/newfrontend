import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import TextAreaAutoSize from "@material-ui/core/TextareaAutosize";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";

import './contact.css';





const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',


        },
        
        paddingTop: "200px"
    },
    contact: {
        width: theme.spacing(50)
    },
    textArea: {
        width: theme.spacing(50),
        backgroundColor:"#535453"
        
    },
   
}));

export default function ContactComponent() {
    const classes = useStyles();
    return (
        <div  className="centreContact">
            
            <Typography variant="h3">Contact Me</Typography>
            <Divider></Divider>
            <form className={classes.root}  autoComplete="off">
                <TextField className={classes.contact} label="Name" /><br></br>
                <TextField className={classes.contact} label="Email" /><br></br><br></br>
                <TextAreaAutoSize className={classes.textArea} rowsMin={3} placeholder="Your Message" /><br></br><br></br>
                <Button
                    variant="contained"
                    color="primary"
                >Submit</Button>

            </form>
            <div>
            </div>
        </div>

    )
}
