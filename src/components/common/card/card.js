import React, { Component } from 'react';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
// import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';

export default class CardComponent extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <>
                <Card>
                    <CardHeader>{this.props.person.name}</CardHeader>
                    <CardMedia>{this.props.person.image}</CardMedia>
                    <CardContent>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {this.props.person.description}
                        </Typography>
                    </CardContent>
                    {/* <CardActions></CardActions> */}
                </Card>
            </>
        )
    }
}
